({
	doInit : function(component, event, helper){
        
		var getDefaults =  component.get('c.getDefault');
		var SDate = component.get('v.selectedStartDate');
		var EDate = component.get('v.selectedEndDate');
		
		if(SDate > EDate ){
		    alert('Enter valid Date');
		    component.set("v.Spinner", false);
		}
		else{
    		getDefaults.setParams({
    			'StartDate'	: SDate,
                'EndDate' : EDate
    		});
    		getDefaults.setCallback(this, function(response){
    			var state = response.getState();
    			if(state == 'SUCCESS'){
    				component.set('v.OppFieldHistory',response.getReturnValue());
    				component.set("v.Spinner", false);
    			}else{
    				alert('Fail');
    				component.set("v.Spinner", false);
    			}
    		});
    		$A.enqueueAction(getDefaults);
		}
	},
})