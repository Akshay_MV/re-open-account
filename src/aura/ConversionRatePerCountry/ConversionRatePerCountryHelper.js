({
    
    doInit : function(component, event, helper){
        component.set("v.Spinner", true);
        var today = $A.localizationService.formatDate(new Date(), "YYYY-MM-DD");
        var selectedYear    = today[0]+today[1]+today[2]+today[3];
        component.set("v.selectedYear", selectedYear);
        
        if(component.get('v.typeOfTable') == 'Number'){
            helper.getData(component, event, helper);
        }else{
            helper.getDataPercentage(component, event, helper);
        }
    },
    
    getUserNameData : function(component, event, helper){
        var action = component.get('c.UserName');
        action.setParams({
            Year : component.get('v.selectedYear')
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                component.set('v.userNames',response.getReturnValue());
                console.log(response.getReturnValue());
                // alert('SUCCESS');
                component.set("v.Spinner", false);
            }else{
                alert('Something went wrong!');
                component.set("v.Spinner", false);
            }
        });
        $A.enqueueAction(action);
    },
    
    getData : function(component, event, helper){
        component.set('v.dataWrapper',[]);
        var action = component.get('c.countryData');
        action.setParams({
            Year : component.get('v.selectedYear'),
            userNames : component.get('v.selectedCheckBoxes')
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if('SUCCESS' == state){
                // alert('SUCCESS');
                component.set('v.dataWrapper',response.getReturnValue());
                
                var responseData = response.getReturnValue();
                helper.getTotal(component, event, helper);
                
                var TotalAQualificationCount = 0;
                var TotalBDiscoveryCount = 0;
                var TotalCPOVCount = 0;
                var TotalDClosingCount = 0;
                var TotalFHandOverCount = 0;
                var TotalEClosedWonCount = 0;
                var TotalClosedDeadCount = 0;
                
                for(var i = 0 ; i < response.getReturnValue().length ; i++){
                    
                    TotalAQualificationCount    += response.getReturnValue()[i].TotalAQualificationCount;
                    TotalBDiscoveryCount        += response.getReturnValue()[i].TotalBDiscoveryCount;
                    TotalCPOVCount              += response.getReturnValue()[i].TotalCPOVCount;
                    TotalDClosingCount          += response.getReturnValue()[i].TotalDClosingCount;
                    TotalFHandOverCount         += response.getReturnValue()[i].TotalFHandOverCount;
                    TotalEClosedWonCount        += response.getReturnValue()[i].TotalEClosedWonCount;
                    TotalClosedDeadCount        += response.getReturnValue()[i].TotalClosedDeadCount;
                }
                
                console.log('CR ==>'+(TotalBDiscoveryCount/TotalAQualificationCount) * 100);
                
                component.set('v.TotalAQualificationCount',TotalAQualificationCount);
                component.set('v.TotalBDiscoveryCount',TotalBDiscoveryCount);
                component.set('v.TotalCPOVCount',TotalCPOVCount);
                component.set('v.TotalDClosingCount',TotalDClosingCount);
                component.set('v.TotalFHandOverCount',TotalFHandOverCount);
                component.set('v.TotalEClosedWonCount',TotalEClosedWonCount);
                component.set('v.TotalClosedDeadCount',TotalClosedDeadCount);
                
                
                
                // Percentage
                
                component.set('v.PerTotalAQualificationCount',  ((TotalBDiscoveryCount/TotalAQualificationCount) * 100).toFixed(2));
                component.set('v.PerTotalBDiscoveryCount',      ((TotalCPOVCount/TotalBDiscoveryCount) * 100).toFixed(2));
                component.set('v.PerTotalCPOVCount',            ((TotalDClosingCount/TotalCPOVCount) * 100).toFixed(2));
                component.set('v.PerTotalDClosingCount',        ((TotalFHandOverCount/TotalDClosingCount) * 100).toFixed(2));
                component.set('v.PerTotalFHandOverCount',       ((TotalEClosedWonCount/TotalFHandOverCount) * 100).toFixed(2));
                component.set('v.PerTotalEClosedWonCount',      ((TotalEClosedWonCount/TotalAQualificationCount) * 100).toFixed(2));
                // component.set('v.PerTotalClosedDeadCount',      (TotalBDiscoveryCount/TotalAQualificationCount) * 100));
                
                
                component.set("v.Spinner", false);
                component.set("v.isModalOpen", false);
            }else{
                alert('FAIL');
                component.set("v.Spinner", false);
            }
            
        });
        $A.enqueueAction(action);
    },
    
    getDataPercentage : function(component, event, helper){
        component.set('v.dataWrapper',[]);
        var action = component.get('c.mainPer');
        action.setParams({
            Year : component.get('v.selectedYear'),
            userNames : component.get('v.selectedCheckBoxes')
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if('SUCCESS' == state){
                // alert('SUCCESS');
                component.set('v.dataWrapper',response.getReturnValue());
                helper.getTotal(component, event, helper);
                console.log(response.getReturnValue());
                component.set("v.Spinner", false);
            }else{
                alert('FAIL');
                component.set("v.Spinner", false);
            }
            
        });
        $A.enqueueAction(action);
    },
    
    getTotal : function(component, event, helper){
        var action = component.get('c.main');
        action.setParams({
            Year : component.get('v.selectedYear'),
            userNames : component.get('v.selectedCheckBoxes')
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if('SUCCESS' == state){
                // alert('SUCCESS');
                component.set('v.oppCount',response.getReturnValue().OppCount);
                component.set('v.conversionRate',response.getReturnValue().ConversionRate);
                
                component.set('v.totalWon',response.getReturnValue().TotalWon);
                component.set('v.totalLost',response.getReturnValue().TotalLost);
                console.log(response.getReturnValue().TotalWon);
                component.set("v.Spinner", false);
            }else{
                alert('FAIL');
                component.set("v.Spinner", false);
            }
            
        });
        $A.enqueueAction(action);
    },
    
	getPickListValues : function(component, event, helper, objectName, FieldName, AttributName){
        var status = component.get("c.getPickListValues");
        status.setParams({
            'ObjName' : objectName,
            'FieldName' : FieldName
        });
        status.setCallback(this, function(response) {
            var allValues = response.getReturnValue();
            component.set("v."+AttributName, allValues);
            component.set("v.Spinner", false);
        });
        $A.enqueueAction(status);
    }
})