({
	init : function(component, event, helper) {
		var leadId = component.get("v.recordId");
        
        var action = component.get("c.getRecordDetail");
	    
        action.setParams({ 
            recordId : leadId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                component.set("v.industryName", data.recordName);
                component.set("v.industryId", data.recordId);
            }
        });
        $A.enqueueAction(action);
	},
    
    saveData : function(component, event, helper){
        var leadId = component.get("v.recordId");
        var industryId = component.get("v.industryId");
        
        var action = component.get("c.saveRecordDetail");
	    
        action.setParams({ 
            recordId : leadId,
            industryId : industryId
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var data = response.getReturnValue();
                helper.NotificationToast(component, event, helper, 'Success!', 'Success', 'Industry Updated on lead');
            }
        });
        $A.enqueueAction(action);
    }
})