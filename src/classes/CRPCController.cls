/*************************************************************************
AUTHOR      : AKSHAY CHAUHAN
DATE        : 03/10/2019

Used by Lightning Component  : ConversionRatePerCountry
**************************************************************************/
public class CRPCController {
    
    @AuraEnabled
    public static List<String> UserName(Integer Year){
        List<Opportunity> OppList = new List<Opportunity>();
        OppList = [ SELECT Id, Name, StageName, Country__c, CreatedDate, Account.BillingCountry, Account.Name, Responsible_Sales__c, Responsible_Sales__r.Name
                      FROM Opportunity 
                     WHERE CALENDAR_YEAR(CreatedDate) =: Year 
                    //   AND ( Responsible_Sales__r.Name = 'Dharmik Shah' 
                    //     OR   Responsible_Sales__r.Name = 'Luca Haag' )
                     ];
        Set<String> userName = new Set<String>();         
        for(Opportunity op : OppList){
            userName.add(op.Responsible_Sales__r.Name);
        }
        
        List<String> check = new List<String>();
        check.addAll(userName);
        check.add('No Responsible Sales');
        System.debug(check);
        return check;
    }
    
    @AuraEnabled
    public static List<countryWrapper> countryData(Integer Year, List<String> userNames){
        System.debug('U name'+userNames);
        String UN = '';
        for(Integer i = 0 ; i < userNames.size() ; i++ ){
            if(userNames[i] == null || userNames[i] == ''){
                userNames.remove(i);
            }
        }
        for(Integer i = 0 ; i < userNames.size() ; i++ ){
            if(i == 0){
                UN += 'AND ( Responsible_Sales__r.Name = \'';
                if(userNames[i] == 'No Responsible Sales'){
                    UN += '';
                }else{
                    UN += userNames[i];
                }
                UN += '\'';
            }else if(i-2 != userNames.size() ){
                UN += ' OR Responsible_Sales__r.Name = \'';
                if(userNames[i] == 'No Responsible Sales'){
                    UN += '';
                }else{
                    UN += userNames[i];
                }
                UN += '\'';
            }
        }
        UN += ' )';
        System.debug(UN);
        
        List<Opportunity> OppList = new List<Opportunity>();
        
        String OppListString = ' SELECT Id, Name, StageName, Country__c, CreatedDate, Account.BillingCountry, Account.Name, Responsible_Sales__c, Responsible_Sales__r.Name '+
                              +' FROM Opportunity  '
                              +' WHERE CALENDAR_YEAR(CreatedDate) =: Year ';
                              if(!userNames.isEmpty()){
                                  OppListString += UN;
                              }
        
        OppList = Database.query(OppListString);
        
        /*OppList = [ SELECT Id, Name, StageName, Country__c, CreatedDate, Account.BillingCountry, Account.Name, Responsible_Sales__c, Responsible_Sales__r.Name
                      FROM Opportunity 
                 WHERE CALENDAR_YEAR(CreatedDate) =: Year
                     ];*/
                     
        // Country A
                    // Name A   // Data A1, Data A2, Data A3
                    // Name B   // Data B1, Data B2, Data B3
       
        Map<String, Map<String, personWrapper>> salesMap = new Map<String, Map<String, personWrapper>>();
        
        for(Opportunity opp : OppList){
            
            if(!salesMap.containsKey(opp.Country__c)){
                Map<String, personWrapper> personMap = new Map<String, personWrapper>();
                if(!personMap.containsKey(opp.Responsible_Sales__r.Name)){
                    personWrapper pw = new personWrapper();
                    pw.SalesPersonName = opp.Responsible_Sales__r.Name == null ? 'No Responsible Sales' : opp.Responsible_Sales__r.Name;
                    
                    if(opp.StageName == 'A Qualification'){
                        pw.AQualificationCount += 1;
                    }
                    if(opp.StageName == 'B Discovery'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                    }
                    if(opp.StageName == 'C POV'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                    }
                    if(opp.StageName == 'D Closing'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                        pw.DClosingCount        += 1;
                    }
                    if(opp.StageName == 'Contract Replaced' || opp.StageName == 'Request Handover' || opp.StageName == 'Contract Signing' || opp.StageName == 'Contract scanned'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                        pw.DClosingCount        += 1;
                        pw.FHandOverCount       += 1;
                    }
                    if(opp.StageName == 'E Closed Won'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                        pw.DClosingCount        += 1;
                        pw.FHandOverCount       += 1;
                        pw.EClosedWonCount      += 1;
                    }
                    if(opp.StageName == 'Closed Dead'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                        pw.DClosingCount        += 1;
                        // dw.FHandOverCount       += 1;
                        pw.ClosedDeadCount      += 1;
                    }
                    if(opp.StageName == 'Terminated'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                        pw.DClosingCount        += 1;
                        // dw.FHandOverCount       += 1;
                    }   
                    
                    personMap.put(opp.Responsible_Sales__r.Name , pw);
                }else{
                    personWrapper pw = personMap.get(opp.Responsible_Sales__r.Name);
                    
                    if(opp.StageName == 'A Qualification'){
                        pw.AQualificationCount += 1;
                    }
                    if(opp.StageName == 'B Discovery'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                    }
                    if(opp.StageName == 'C POV'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                    }
                    if(opp.StageName == 'D Closing'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                        pw.DClosingCount        += 1;
                    }
                    if(opp.StageName == 'Contract Replaced' || opp.StageName == 'Request Handover' || opp.StageName == 'Contract Signing' || opp.StageName == 'Contract scanned'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                        pw.DClosingCount        += 1;
                        pw.FHandOverCount       += 1;
                    }
                    if(opp.StageName == 'E Closed Won'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                        pw.DClosingCount        += 1;
                        pw.FHandOverCount       += 1;
                        pw.EClosedWonCount      += 1;
                    }
                    if(opp.StageName == 'Closed Dead'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                        pw.DClosingCount        += 1;
                        // dw.FHandOverCount       += 1;
                        pw.ClosedDeadCount      += 1;
                    }
                    if(opp.StageName == 'Terminated'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                        pw.DClosingCount        += 1;
                        // dw.FHandOverCount       += 1;
                    }
                    
                    personMap.put(opp.Responsible_Sales__r.Name, pw);
                }
                // personWrapper pw = personMap.get(opp.Responsible_Sales__r.Name);
                // pw.TotalAQualificationCount += 1;
                // personMap.put(opp.Responsible_Sales__r.Name, pw);
                
                salesMap.put(opp.Country__c == null ? 'No Country' : opp.Country__c , personMap);
            }else{
                Map<String, personWrapper> personMap = salesMap.get(opp.Country__c);
                if(!personMap.containsKey(opp.Responsible_Sales__r.Name)){
                    personWrapper pw = new personWrapper();
                    if(opp.StageName == 'A Qualification'){
                        pw.AQualificationCount += 1;
                    }
                    if(opp.StageName == 'B Discovery'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                    }
                    if(opp.StageName == 'C POV'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                    }
                    if(opp.StageName == 'D Closing'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                        pw.DClosingCount        += 1;
                    }
                    if(opp.StageName == 'Contract Replaced' || opp.StageName == 'Request Handover' || opp.StageName == 'Contract Signing' || opp.StageName == 'Contract scanned'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                        pw.DClosingCount        += 1;
                        pw.FHandOverCount       += 1;
                    }
                    if(opp.StageName == 'E Closed Won'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                        pw.DClosingCount        += 1;
                        pw.FHandOverCount       += 1;
                        pw.EClosedWonCount      += 1;
                    }
                    if(opp.StageName == 'Closed Dead'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                        pw.DClosingCount        += 1;
                        // dw.FHandOverCount       += 1;
                        pw.ClosedDeadCount      += 1;
                    }
                    if(opp.StageName == 'Terminated'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                        pw.DClosingCount        += 1;
                        // dw.FHandOverCount       += 1;
                    }
                    pw.SalesPersonName = opp.Responsible_Sales__r.Name == null ? 'No Responsible Sales' : opp.Responsible_Sales__r.Name;
                    
                    // pw.Qualification += 1;
                    personMap.put(opp.Responsible_Sales__r.Name, pw);
                }else{
                    personWrapper pw = personMap.get(opp.Responsible_Sales__r.Name);
                    if(opp.StageName == 'A Qualification'){
                        pw.AQualificationCount += 1;
                    }
                    if(opp.StageName == 'B Discovery'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                    }
                    if(opp.StageName == 'C POV'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                    }
                    if(opp.StageName == 'D Closing'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                        pw.DClosingCount        += 1;
                    }
                    if(opp.StageName == 'Contract Replaced' || opp.StageName == 'Request Handover' || opp.StageName == 'Contract Signing' || opp.StageName == 'Contract scanned'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                        pw.DClosingCount        += 1;
                        pw.FHandOverCount       += 1;
                    }
                    if(opp.StageName == 'E Closed Won'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                        pw.DClosingCount        += 1;
                        pw.FHandOverCount       += 1;
                        pw.EClosedWonCount      += 1;
                    }
                    if(opp.StageName == 'Closed Dead'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                        pw.DClosingCount        += 1;
                        // dw.FHandOverCount       += 1;
                        pw.ClosedDeadCount      += 1;
                    }
                    if(opp.StageName == 'Terminated'){
                        pw.AQualificationCount  += 1;
                        pw.BDiscoveryCount      += 1;
                        pw.CPOVCount            += 1;
                        pw.DClosingCount        += 1;
                        // dw.FHandOverCount       += 1;
                    }
                    // pw.Qualification += 1;
                    personMap.put(opp.Responsible_Sales__r.Name, pw);
                }
                
                // personWrapper pw = personMap.get(opp.Responsible_Sales__r.Name);
                // pw.TotalAQualificationCount += 1;
                // personMap.put(opp.Responsible_Sales__r.Name, pw);
                
                salesMap.put(opp.Country__c == null ? 'No Country' : opp.Country__c, personMap);
            }
        }
        
        List<countryWrapper> cwList = new List<countryWrapper>();
    
        for(String key : salesMap.keyset()){
            countryWrapper cw = new countryWrapper();
            cw.ContryName = key;
            
            
            
            Map<String, personWrapper> personMap = salesMap.get(key);
            List<personWrapper> pwl = new List<personWrapper>();
            for(String secKey : personMap.keyset()){
                pwl.add(personMap.get(secKey));
                
                cw.TotalAQualificationCount += personMap.get(secKey).AQualificationCount;
                cw.TotalBDiscoveryCount     += personMap.get(secKey).BDiscoveryCount;
                cw.TotalCPOVCount           += personMap.get(secKey).CPOVCount;
                cw.TotalDClosingCount       += personMap.get(secKey).DClosingCount;
                cw.TotalFHandOverCount      += personMap.get(secKey).FHandOverCount;
                cw.TotalEClosedWonCount     += personMap.get(secKey).EClosedWonCount;
                cw.TotalClosedDeadCount     += personMap.get(secKey).ClosedDeadCount;
            }
            cw.personList = pwl;
            cwList.add(cw);
        }
        return cwList;
    }
    
    @AuraEnabled
    public static List<countryWrapperPer> mainPer(Integer Year, List<String> userNames){
        List<countryWrapper> contryList = countryData(Year, userNames);
        List<countryWrapperPer> cwpList = new List<countryWrapperPer>();
        
        for(countryWrapper cw : contryList){
            
            List<personWrapperPer> pwList = new List<personWrapperPer>();
            
            countryWrapperPer cwp = new countryWrapperPer();    
            cwp.ContryName = cw.ContryName;
            
            for(personWrapper pw : cw.personList){
                personWrapperPer pwp = new personWrapperPer();
                pwp.SalesPersonName     = pw.SalesPersonName;
                
                Decimal AQualificationCount = 0.00;
                Decimal BDiscoveryCount     = 0.00;
                Decimal CPOVCount           = 0.00;
                Decimal DClosingCount       = 0.00;
                Decimal FHandOverCount      = 0.00;
                Decimal EClosedWonCount     = 0.00;
                Decimal ClosedDeadCount     = 0.00;
                Decimal TerminatedCount     = 0.00;
                
                if(pw.AQualificationCount != 0)     AQualificationCount = (decimal.valueOf(pw.BDiscoveryCount)  * 100)  / decimal.valueOf(pw.AQualificationCount);
                if(pw.BDiscoveryCount != 0)         BDiscoveryCount     = (decimal.valueOf(pw.CPOVCount)        * 100)  / decimal.valueOf(pw.BDiscoveryCount).setScale(2);
                if(pw.CPOVCount != 0)               CPOVCount           = (decimal.valueOf(pw.DClosingCount)    * 100)  / decimal.valueOf(pw.CPOVCount).setScale(2);
                if(pw.DClosingCount != 0)           DClosingCount       = (decimal.valueOf(pw.FHandOverCount)   * 100)  / decimal.valueOf(pw.DClosingCount).setScale(2);
                if(pw.FHandOverCount != 0)          FHandOverCount      = (decimal.valueOf(pw.EClosedWonCount)  * 100)  / decimal.valueOf(pw.FHandOverCount).setScale(2);
                if(pw.AQualificationCount != 0)     EClosedWonCount     = (decimal.valueOf(pw.EClosedWonCount)  * 100)  / decimal.valueOf(pw.AQualificationCount).setScale(2);
                if(pw.AQualificationCount != 0)     ClosedDeadCount     = (decimal.valueOf(pw.BDiscoveryCount)  * 100)  / decimal.valueOf(pw.AQualificationCount).setScale(2);
                if(pw.AQualificationCount != 0)     TerminatedCount     = (decimal.valueOf(pw.BDiscoveryCount)  * 100)  / decimal.valueOf(pw.AQualificationCount).setScale(2);
                
                pwp.AQualificationCount = AQualificationCount.setScale(2);
                pwp.BDiscoveryCount     = BDiscoveryCount.setScale(2);
                pwp.CPOVCount           = CPOVCount.setScale(2);
                pwp.DClosingCount       = DClosingCount.setScale(2);
                pwp.FHandOverCount      = FHandOverCount.setScale(2);
                pwp.EClosedWonCount     = EClosedWonCount.setScale(2);
                pwp.ClosedDeadCount     = ClosedDeadCount.setScale(2);
                pwp.TerminatedCount     = TerminatedCount.setScale(2);
                
                pwList.add(pwp);
            }
            cwp.personList = pwList;
            
            Decimal TotalAQualificationCount    = cw.TotalAQualificationCount   != 0 ? (decimal.valueOf(cw.TotalBDiscoveryCount)    * 100) / decimal.valueOf(cw.TotalAQualificationCount)   : 0.00;
            Decimal TotalBDiscoveryCount        = cw.TotalBDiscoveryCount       != 0 ? (decimal.valueOf(cw.TotalCPOVCount)          * 100) / decimal.valueOf(cw.TotalBDiscoveryCount)       : 0.00;
            Decimal TotalCPOVCount              = cw.TotalCPOVCount             != 0 ? (decimal.valueOf(cw.TotalDClosingCount)      * 100) / decimal.valueOf(cw.TotalCPOVCount)             : 0.00;
            Decimal TotalDClosingCount          = cw.TotalDClosingCount         != 0 ? (decimal.valueOf(cw.TotalFHandOverCount)     * 100) / decimal.valueOf(cw.TotalDClosingCount)         : 0.00;
            Decimal TotalFHandOverCount         = cw.TotalFHandOverCount        != 0 ? (decimal.valueOf(cw.TotalEClosedWonCount)    * 100) / decimal.valueOf(cw.TotalFHandOverCount)        : 0.00;
            Decimal TotalEClosedWonCount        = cw.TotalEClosedWonCount       != 0 ? (decimal.valueOf(cw.TotalEClosedWonCount)    * 100) / decimal.valueOf(cw.TotalAQualificationCount)   : 0.00;
            
            cwp.TotalAQualificationCount    = TotalAQualificationCount.setScale(2);
            cwp.TotalBDiscoveryCount        = TotalBDiscoveryCount.setScale(2);
            cwp.TotalCPOVCount              = TotalCPOVCount.setScale(2);
            cwp.TotalDClosingCount          = TotalDClosingCount.setScale(2);
            cwp.TotalFHandOverCount         = TotalFHandOverCount.setScale(2);
            cwp.TotalEClosedWonCount        = TotalEClosedWonCount.setScale(2);
            
            cwpList.add(cwp);
        }
        return cwpList;
    }
    
    public class countryWrapperPer{
        @AuraEnabled public String ContryName;
        @AuraEnabled public List<personWrapperPer> personList;
        
        //TOTAL
        @AuraEnabled public decimal TotalAQualificationCount = 0;
        @AuraEnabled public decimal TotalBDiscoveryCount = 0;
        @AuraEnabled public decimal TotalCPOVCount = 0;
        @AuraEnabled public decimal TotalDClosingCount = 0;
        @AuraEnabled public decimal TotalFHandOverCount = 0;
        @AuraEnabled public decimal TotalEClosedWonCount = 0;
        @AuraEnabled public decimal TotalClosedDeadCount = 0;
        @AuraEnabled public decimal TotalTerminatedCount = 0;
        @AuraEnabled public decimal TotalClosedWonProBonoCount = 0;
        
    }
    
    public class personWrapperPer{
        @AuraEnabled public String SalesPersonName;
        @AuraEnabled public Integer Qualification = 0;
        
        @AuraEnabled public decimal AQualificationCount = 0;
        @AuraEnabled public decimal BDiscoveryCount = 0;
        @AuraEnabled public decimal CPOVCount = 0;
        @AuraEnabled public decimal DClosingCount = 0;
        @AuraEnabled public decimal FHandOverCount = 0;
        @AuraEnabled public decimal EClosedWonCount = 0;
        @AuraEnabled public decimal ClosedDeadCount = 0;
        @AuraEnabled public decimal TerminatedCount = 0;
        @AuraEnabled public decimal ClosedWonProBonoCount = 0;
    }
    
    public class countryWrapper{
        @AuraEnabled public String ContryName;
        @AuraEnabled public List<personWrapper> personList;
        
        // TOTAL Values
        @AuraEnabled public Integer TotalAQualificationCount = 0;
        @AuraEnabled public Integer TotalBDiscoveryCount = 0;
        @AuraEnabled public Integer TotalCPOVCount = 0;
        @AuraEnabled public Integer TotalDClosingCount = 0;
        @AuraEnabled public Integer TotalFHandOverCount = 0;
        @AuraEnabled public Integer TotalEClosedWonCount = 0;
        @AuraEnabled public Integer TotalClosedDeadCount = 0;
        @AuraEnabled public Integer TotalTerminatedCount = 0;
        @AuraEnabled public Integer TotalClosedWonProBonoCount = 0;
        
    }
    
    public class personWrapper{
        @AuraEnabled public String SalesPersonName;
        @AuraEnabled public Integer Qualification = 0;
        
        @AuraEnabled public Integer AQualificationCount = 0;
        @AuraEnabled public Integer BDiscoveryCount = 0;
        @AuraEnabled public Integer CPOVCount = 0;
        @AuraEnabled public Integer DClosingCount = 0;
        @AuraEnabled public Integer FHandOverCount = 0;
        @AuraEnabled public Integer EClosedWonCount = 0;
        @AuraEnabled public Integer ClosedDeadCount = 0;
        @AuraEnabled public Integer TerminatedCount = 0;
        @AuraEnabled public Integer ClosedWonProBonoCount = 0;
    }
    
    @AuraEnabled
    public static CountyListWrapper main(Integer Year, List<String> userNames){
        
        List<Opportunity>             OppList        = new List<Opportunity>();
        List<OpportunityFieldHistory> oppHistoryList = new List<OpportunityFieldHistory>();
        List<String>                  stageList      = new List<String>();
        Integer cr = 0;
        
        DataWrapper dwList = new DataWrapper();
        
        
        String UN = '';
        for(Integer i = 0 ; i < userNames.size() ; i++ ){
            if(userNames[i] == null || userNames[i] == ''){
                userNames.remove(i);
            }
        }
        for(Integer i = 0 ; i < userNames.size() ; i++ ){
            if(i == 0){
                UN += 'AND ( Responsible_Sales__r.Name = \'';
                if(userNames[i] == 'No Responsible Sales'){
                    UN += '';
                }else{
                    UN += userNames[i];
                }
                UN += '\'';
            }else if(i-2 != userNames.size() ){
                UN += ' OR Responsible_Sales__r.Name = \'';
                if(userNames[i] == 'No Responsible Sales'){
                    UN += '';
                }else{
                    UN += userNames[i];
                }
                UN += '\'';
            }
        }
        UN += ' )';
        System.debug(UN);
        
        // List<Opportunity> OppList = new List<Opportunity>();
        
        String OppListString = ' SELECT Id, Name, StageName, Country__c, CreatedDate, Account.BillingCountry, Account.Name, Responsible_Sales__c, Responsible_Sales__r.Name '+
                              +' FROM Opportunity  '
                              +' WHERE CALENDAR_YEAR(CreatedDate) =: Year ';
                              if(!userNames.isEmpty()){
                                  OppListString += UN;
                              }
        
        OppList = Database.query(OppListString);
        
        
        /*OppList = [ SELECT Id, Name, StageName, Country__c, CreatedDate, Account.BillingCountry, Account.Name, Responsible_Sales__c, Responsible_Sales__r.Name
                      FROM Opportunity 
                     WHERE CALENDAR_YEAR(CreatedDate) =: Year ];*/
        
        CountyListWrapper ofh   = new CountyListWrapper();
        Map<String, DataWrapper> crpcMap = new Map<String, DataWrapper>();
        List<CountyListWrapper> clwList = new List<CountyListWrapper>();
        
        for(Opportunity oph : OppList){
            
            if(oph.StageName == 'E Closed Won'){
                cr += 1;
            }
            if(!crpcMap.containsKey(oph.Account.BillingCountry)){
                DataWrapper dw = new DataWrapper();
                
                if(oph.StageName == 'A Qualification'){
                    dw.AQualificationCount += 1;
                }
                if(oph.StageName == 'B Discovery'){
                    dw.AQualificationCount  += 1;
                    dw.BDiscoveryCount      += 1;
                }
                if(oph.StageName == 'C POV'){
                    dw.AQualificationCount  += 1;
                    dw.BDiscoveryCount      += 1;
                    dw.CPOVCount            += 1;
                }
                if(oph.StageName == 'D Closing'){
                    dw.AQualificationCount  += 1;
                    dw.BDiscoveryCount      += 1;
                    dw.CPOVCount            += 1;
                    dw.DClosingCount        += 1;
                }
                if(oph.StageName == 'Contract Replaced' || oph.StageName == 'Request Handover' || oph.StageName == 'Contract Signing' || oph.StageName == 'Contract scanned'){
                    dw.AQualificationCount  += 1;
                    dw.BDiscoveryCount      += 1;
                    dw.CPOVCount            += 1;
                    dw.DClosingCount        += 1;
                    dw.FHandOverCount       += 1;
                }
                if(oph.StageName == 'E Closed Won'){
                    dw.AQualificationCount  += 1;
                    dw.BDiscoveryCount      += 1;
                    dw.CPOVCount            += 1;
                    dw.DClosingCount        += 1;
                    dw.FHandOverCount       += 1;
                    dw.EClosedWonCount      += 1;
                }
                if(oph.StageName == 'Closed Dead'){
                    dw.AQualificationCount  += 1;
                    dw.BDiscoveryCount      += 1;
                    dw.CPOVCount            += 1;
                    dw.DClosingCount        += 1;
                    // dw.FHandOverCount       += 1;
                    dw.ClosedDeadCount      += 1;
                }
                if(oph.StageName == 'Terminated'){
                    dw.AQualificationCount  += 1;
                    dw.BDiscoveryCount      += 1;
                    dw.CPOVCount            += 1;
                    dw.DClosingCount        += 1;
                    // dw.FHandOverCount       += 1;
                }
                
                dw.Name = oph.Responsible_Sales__r.Name;
                dw.CountryName = oph.Account.BillingCountry != '' ? oph.Account.BillingCountry : 'Without Country';
                ofh.CountryName = oph.Account.BillingCountry != '' ? oph.Account.BillingCountry : 'Without Country';
                System.debug(' C N ==>>'+ofh.CountryName);
                ofh.OppCount = OppList.size();
                
                
                Decimal conrate = 0.00;
                if(OppList.size() != 0) conrate = (( 100 * decimal.valueOf(cr) ) / decimal.valueOf(ofh.OppCount));
                
                
                ofh.ConversionRate = conrate.setScale(2);
                ofh.DataListWrapper = crpcMap.values();
                crpcMap.put(oph.Account.BillingCountry, dw); 
                if(oph.StageName == 'E Closed Won') ofh.TotalWon += 1;
                if(oph.StageName == 'Closed Dead') ofh.TotalLost += 1;
            }else{
                DataWrapper dw = crpcMap.get(oph.Account.BillingCountry);
                
                if(oph.StageName == 'A Qualification'){
                    dw.AQualificationCount += 1;
                }
                if(oph.StageName == 'B Discovery'){
                    dw.AQualificationCount  += 1;
                    dw.BDiscoveryCount      += 1;
                }
                if(oph.StageName == 'C POV'){
                    dw.AQualificationCount  += 1;
                    dw.BDiscoveryCount      += 1;
                    dw.CPOVCount            += 1;
                }
                if(oph.StageName == 'D Closing'){
                    dw.AQualificationCount  += 1;
                    dw.BDiscoveryCount      += 1;
                    dw.CPOVCount            += 1;
                    dw.DClosingCount        += 1;
                }
                if(oph.StageName == 'Contract Replaced' || oph.StageName == 'Request Handover' || oph.StageName == 'Contract Signing' || oph.StageName == 'Contract scanned'){
                    dw.AQualificationCount  += 1;
                    dw.BDiscoveryCount      += 1;
                    dw.CPOVCount            += 1;
                    dw.DClosingCount        += 1;
                    dw.FHandOverCount       += 1;
                }
                if(oph.StageName == 'E Closed Won'){
                    dw.AQualificationCount  += 1;
                    dw.BDiscoveryCount      += 1;
                    dw.CPOVCount            += 1;
                    dw.DClosingCount        += 1;
                    dw.FHandOverCount       += 1;
                    dw.EClosedWonCount      += 1;
                }
                if(oph.StageName == 'Closed Dead'){
                    dw.AQualificationCount  += 1;
                    dw.BDiscoveryCount      += 1;
                    dw.CPOVCount            += 1;
                    dw.DClosingCount        += 1;
                    // dw.FHandOverCount       += 1;
                    dw.ClosedDeadCount      += 1;
                }
                if(oph.StageName == 'Terminated'){
                    dw.AQualificationCount  += 1;
                    dw.BDiscoveryCount      += 1;
                    dw.CPOVCount            += 1;
                    dw.DClosingCount        += 1;
                    // dw.FHandOverCount       += 1;
                }
                
                dw.CountryName = oph.Account.BillingCountry;
                ofh.CountryName = oph.Account.BillingCountry;
                if(oph.StageName == 'E Closed Won') ofh.TotalWon += 1;
                if(oph.StageName == 'Closed Dead') ofh.TotalLost += 1;
                crpcMap.put(oph.Account.BillingCountry, dw);
            }
        }
        clwList.add(ofh);
        return ofh;
    }
    
    // METHOD FOR GETTING PICK-LIST VALUES FROM BACKEND
    @AuraEnabled
    public static List<String> getPickListValues(String ObjName,String FieldName){
        String[] values = new String[]{};
        String[] types = new String[]{ObjName};
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(types);
        
        // values.add('--None--');
        for(Schema.DescribeSobjectResult res : results) {
            for (Schema.PicklistEntry entry : res.fields.getMap().get(FieldName).getDescribe().getPicklistValues()) {
                if (entry.isActive()) {values.add(entry.getValue());}
            }
        }
        return values;
    }
    
    public class CountyListWrapper{
        @AuraEnabled public String CountryName;
        @AuraEnabled public Integer OppCount = 0;
        @AuraEnabled public Double ConversionRate = 0.00;
        @AuraEnabled public Integer TotalWon = 0;
        @AuraEnabled public Integer TotalLost = 0;
        @AuraEnabled public List<DataWrapper> DataListWrapper;
    }
    
    public class DataWrapper{
        @AuraEnabled public String Name;
        @AuraEnabled public String StageName;
        @AuraEnabled public String StageNameOld;
        @AuraEnabled public String CountryName;
        
        @AuraEnabled public Integer AQualificationCount = 0;
        @AuraEnabled public Integer BDiscoveryCount = 0;
        @AuraEnabled public Integer CPOVCount = 0;
        @AuraEnabled public Integer DClosingCount = 0;
        @AuraEnabled public Integer FHandOverCount = 0;
        @AuraEnabled public Integer EClosedWonCount = 0;
        @AuraEnabled public Integer ClosedDeadCount = 0;
        @AuraEnabled public Integer TerminatedCount = 0;
        @AuraEnabled public Integer ClosedWonProBonoCount = 0;
        
    }
    
    public class OppDataWrapper{
        @AuraEnabled public Integer OpportunityCount;
        @AuraEnabled public Decimal ConversionRate;
        @AuraEnabled public list<ConversionRateWrapper> CRWrapperList;
    }
    public class ConversionRateWrapper{
        @AuraEnabled public String StageName;
        @AuraEnabled public String Country;
        @AuraEnabled public Integer count;
    }
}