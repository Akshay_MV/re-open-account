/**************************************
 * Author: Akshay Chauhan
 * Date: 01 December 2019    
 * DESCRIPTION: Handler for Opportunity 
 *              Trigger
 ***************************************/

public class OpportunityTriggerHandler {
	
    List<Opportunity> recordNewList = new List<Opportunity>();
    List<Opportunity> recordOldList = new List<Opportunity>();
    Map<Id, Opportunity> recordNewMap = new Map<Id, Opportunity>();
    Map<Id, Opportunity> recordOldMap = new Map<Id, Opportunity>();
    Boolean isInsert, isUpdate, isDelete, isUndelete = false;
    Private Boolean run = true;

    public OpportunityTriggerHandler(List<Opportunity> newList, List<Opportunity> oldList, Map<Id, Opportunity> newMap, Map<Id, Opportunity> oldMap, boolean isInsert, boolean isUpdate, Boolean isDelete, Boolean isUndelete) {
        this.recordNewList = newList;
        this.recordOldList = oldList;
        this.recordNewMap = newMap;
        this.recordOldMap = oldMap;
        this.isInsert = isInsert;
        this.isUpdate = isUpdate;
        this.isDelete = isDelete;
        this.isUndelete = isUndelete;
    }
    
    public void BeforeInsertEvent(){ 
        for(Integer i = 0 ; i < recordNewList.size() ; i++ ){
            if(recordNewList[i].StageName == 'B Discovery') recordNewList[i].SQL__c  = true;
            //if(recordNewList[i].StageName == 'A Qualification') recordNewList[i].IsQualification__c = true;
            //CR_OppStageController.getDefault(0,0,'','');
            
        }
    }
    
    public void BeforeUpdateEvent(){
        for(Integer i = 0 ; i < recordNewList.size() ; i++ ){
            if(recordNewList[i].StageName == 'B Discovery') recordNewList[i].SQL__c  = true;
            //if(recordNewList[i].StageName == 'A Qualification') recordNewList[i].IsQualification__c = true;
            
            
        }
    }
    
    public void BeforeDeleteEvent(){ }
    
    public void AfterInsertEvent(){ }
    
    public void AfterUpdateEvent(){ }
    
    public void AfterDeleteEvent(){ }
    
    public void AfterUndeleteEvent(){ }
}