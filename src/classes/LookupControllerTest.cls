@isTest
private class LookupControllerTest {

	@isTest static void searchRecordTest() {
	    List<String> MoreFields = new List<String>();
        MoreFields.add('saleswings__NumberofLocations__c');
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test';
        opp.StageName = 'Test';
        opp.CloseDate = System.today();
        insert opp;
        
    	LookupController.searchRecord('Lead', 'Name', 'Name' , 'true', MoreFields, 'test', 10,true);
        LookupController.searchRecord('Lead', 'Name', 'Name' , 'true', MoreFields, 'test', 10,false);
       
	}

}